<?php

class Animal{
  public $name;
  public $legs = 4;
  public $cold_blooded = "no";
  public $jump;
  public $yell;
  

  public function __construct($nama){
    $this->name = $nama;
  }
  public function getcold_blooded($blood){
    $this->cold_blooded = $blood;
  }
  public function getjump($lompat){
    $this->jump = $lompat;
  }
  public function getyell($yelyel){
    $this->yell = $yelyel;
  }
}

?>
